from os import walk, remove, path
import datetime
import json
import re

FILENAME_DEFAULT = 'hostlist.txt'
FILENAME_FULL = 'hostlist-full.txt'
NOW = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
FILTER = r'cdn|online payment'
FIRST_LINES = '# DuckDuckGo Tracker Radar hostfile\n# Made suitable for Pi-Hole\n# Generation on: ' + NOW + ' \n\n'

if path.exists(FILENAME_DEFAULT):
  remove(FILENAME_DEFAULT)
if path.exists(FILENAME_FULL):
  remove(FILENAME_FULL)

list_default = open(FILENAME_DEFAULT, 'w')
list_default.write(FIRST_LINES)
list_full = open(FILENAME_FULL, 'w')
list_full.write(FIRST_LINES + '# !!! Use with caution, this list contains all the domains from the tracker radar, including domains categorized as CDN and online payment\n\n')

# Add all the domains from the Domains folder
(_, _, filenames) = next(walk('./domains'))
filenames = sorted(filenames)
for filename in filenames:
  domain = filename.replace('.json', '') + '\n'
  fe = open('./domains/' + filename, 'r')
  jo = json.load(fe)
  if len(jo['categories']) > 0:
    flag = False
    for d in jo['categories']:
      if re.search(FILTER, d, re.IGNORECASE):
        flag = True
        break
    
    if flag:
      list_full.write(domain)
    else:
      list_default.write(domain)
  else:
    list_default.write(domain)
    list_full.write(domain)

list_default.close()
list_full.close()